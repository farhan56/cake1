<div class="services index">

	<table cellpadding="0" cellspacing="0">
	<tr>

			<th><?php echo $this->Paginator->sort('Name','name');?></th>
			<!--
			<th><?php echo $this->Paginator->sort('Provisional Diagnosis','provisional_diagnosis_id');?></th>
			-->

			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($drugs as $drug):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>

		<td><?php echo $drug['Drug']['name']; ?>&nbsp;</td>
		<!--
		<td><?php echo $drug['ProvisionalDiagnosis']['disease_name']; ?>&nbsp;</td>
		-->

		<td class="actions">
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $drug['Drug']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $drug['Drug']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $drug['Drug']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>


	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>


<div class="actions">

	<ul>
		<li><?php echo $this->Html->link(__('Add Drug', true), array('controller' => 'Drug', 'action' => 'add')); ?> </li>
	</ul>
</div>