<div class="services index">

	<table cellpadding="0" cellspacing="0">
	<tr>

			<th><?php echo $this->Paginator->sort('Name','name');?></th>
			<th><?php echo $this->Paginator->sort('Created On','created_on');?></th>
			<th><?php echo $this->Paginator->sort('Updated On','updated_on');?></th>
			<th><?php echo $this->Paginator->sort('Active', 'is_active');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($roles as $user):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>

		<td><?php echo $user['UserRole']['name']; ?>&nbsp;</td>
		<td><?php echo $user['UserRole']['created_on']; ?>&nbsp;</td>
		<td><?php echo $user['UserRole']['updated_on']; ?>&nbsp;</td>
		<td><?php echo $user['UserRole']['is_active'] == 'Y' ? 'Yes': 'No'; ?>&nbsp;</td>

		<td class="actions">
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $user['UserRole']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $user['UserRole']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $user['UserRole']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>


	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>


<div class="actions">

	<ul>
		<li><?php echo $this->Html->link(__('Add User', true), array('controller' => 'UserRole', 'action' => 'add')); ?> </li>
	</ul>
</div>