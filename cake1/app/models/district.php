<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 30/08/2015
 * Time: 6:21 PM
 */

class District extends AppModel{

    var $name = 'District';
    var $useTable = 'hlink_district';
    var $displayField = 'name';
}