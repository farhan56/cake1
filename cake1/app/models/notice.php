<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 01/09/2015
 * Time: 11:16 AM
 */

class Notice extends AppModel{
    var $name = 'Notice';
    var $useTable = 'hlink_notice';

    var $belongsTo = array(
        'User'=>array(
            'className'=>'User',
            'foreignKey'=>'to_user_id'
        )
    );

}