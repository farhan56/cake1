<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 01/09/2015
 * Time: 12:59 PM
 */

class Message extends AppModel{

    var $name = 'Message';
    var $useTable = 'hlink_message';

    var $belongsTo = array(
        'User'=>array(
            'className'=>'User',
            'foreignKey'=>'from_user_id'
        ),
        'User_reciever'=>array(
            'className'=>'User',
            'foreignKey'=>'to_user_id'
        )

    );
}