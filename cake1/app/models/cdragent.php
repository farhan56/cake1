<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 30/08/2015
 * Time: 4:22 PM
 */

class Cdragent extends AppModel{


    //$this->Model->validates = $validates;
    var $name = 'Cdragent';
    public $validate = array(
        'msisdn' => array(
            'rule' => 'Numeric',
            'required' => true,
            'message' => 'Numbers only'
        ),
        'age_year' => array(
            'rule' => array('minLength', 1),
            'required' => true,
            'message' => 'Enter age year',
            'allowEmpty' => false
        ),
        'age_month' => array(
            'rule' => array('minLength', 1),
            'required' => true,
            'message' => 'Enter age month',
            'allowEmpty' => false
        ),
        'note' => array(
            'rule' => array('minLength', 1),
            'required' => true,
            'message' => 'Enter inquiry',
            'allowEmpty' => false
        )
    );
    var $useTable = 'hlink_cdr_agent_info';


    var $displayField = 'id';

    var $belongsTo =array(
        'User'=>array(
            'fields'=>array('name'),
            'className'=>'User',
            'foreignKey'=>'agent_id'
        )
    );



}