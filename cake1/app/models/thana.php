<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 30/08/2015
 * Time: 6:21 PM
 */

class Thana extends AppModel{

    var $name = 'Thana';
    var $useTable = 'hlink_thana';
    var $displayField = 'name';
}