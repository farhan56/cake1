<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 31/08/2015
 * Time: 1:08 PM
 */

class UserRoleController extends AppController{

    var $name = 'UserRole';
    var $uses = array('UserRole','User');

    function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'setup';
    }
    function index(){
        $this->set('roles', $this->paginate());

    }

    function add(){

        if(!empty($this->data)){
            if ($this->UserRole->save($this->data)) {
                $this->Session->setFlash('New Role has been created.',true);
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__('User Role could not be saved. Please, try again.', true));

            }
        }

    }
    public function edit($id){

        $this->UserRole->id = $id;
        if(!empty($this->data)){
            if ($this->UserRole->save($this->data)) {
                $this->Session->setFlash('User Role has been updated.');
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__('User Role could not be updated. Please, try again.', true));
            }

        }else{
            $this->data = $this->UserRole->read();
        }
    }
    public function delete($id){
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for User Role', true));
            $this->redirect(array('action'=>'index'));
        }else if($this->UserRole->delete($id)){
            $this->Session->setFlash('The User Role with id: ' . $id . ' has been deleted.');
            $this->redirect(array('action' => 'index'));
        }else{

            $this->Session->setFlash(__('The Provisional Diagnosis could not be Deleted. Please, try again.', true));
        }
    }

}