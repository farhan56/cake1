<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 01/09/2015
 * Time: 2:57 PM
 */

class ReportsController extends AppController{
    var $name  = 'Reports';
    var $uses  = array('User');

    function beforeFilter(){
        parent::beforeFilter();
        $this->layout = 'default';

    }

    function index(){

        $this->set('messages', $this->paginate());
    }

    function agent_daily_report(){

        if( isset( $this->data['Reports'] ) )
        {
            $thisYear	=	$this->data['Reports']['From']['year'] ;
            $thisMonth	=	$this->data['Reports']['From']['month'] ;
            $thisDay	=	$this->data['Reports']['From']['day'] ;
            $nxtYear	=	$this->data['Reports']['To']['year'] ;
            $nxtMonth	=	$this->data['Reports']['To']['month'] ;
            $nxtDay		=	$this->data['Reports']['To']['day'] ;
            $agent_id   =   $this->data['Reports']['agent_id'];
            $from_year	=	$thisYear	. '-' . $thisMonth	. '-' . $thisDay ;
            $to_year	=	$nxtYear 	. '-' . $nxtMonth 	. '-' . $nxtDay ;

        }
        else
        {
            $from_year  =  $to_year	=  date('Y') . '-' . date('m') . '-' . date('d') ;
            $agent_id = NULL;

        }
        $subData = $this->_QueryAgentDailyReport(3,$from_year,$to_year,$agent_id);
        //debug($subData);
        foreach($subData as $sdata)
        {
            //debug($sdata);
            $agentId[]          = $sdata['agents']['agent_name'] ;
            $date[]             = $sdata['agents']['login_date'] ;
            $talkTime[]         = $sdata['agents']['total_talk_time'] ;
            $leisureHour[]      = $sdata['agents']['leisure_hour'] ;
            $workingHour[]      = $sdata['agents']['working_hour'] ;
            $callsLanded[]      = $sdata['agents']['total_calls_landed'] ;
            $callsTaken[]       = $sdata['agents']['total_calls_taken'] ;
            $loginTime[]        = $sdata['agents']['loginTime'] ;
            $logoutTime[]       = $sdata['agents']['logoutTime'] ;
            $officeTime[]       = $sdata['agents']['officeTime'];
            $offlineTime[]      = $sdata['agents']['offlineTime'];
            $alarms[]           = $sdata['agents']['alarm'] ;

        }
        $this->set(compact( 'date','loginTime', 'logoutTime','officeTime','offlineTime', 'callstatus',  'agentId', 'talkTime', 'leisureHour', 'workingHour', 'callsLanded', 'callsTaken', 'alarms','from_year','to_year')) ;
    }
    function _QueryAgentDailyReport($company_id,$from_year,$to_year,$agent_id){

        $db = ConnectionManager::getDataSource("vcc"); // name of your database connection
        $where_query = null;
        if($this->Session->read('role')=='Doctor'||$this->Session->read('role')=='Agent'){
            $agent_id = $this->Session->read('login_id');

        }
        if($agent_id){
            $where_query = " AND company_agent_id=".$agent_id." ";
        }
        $query = "  SELECT login_date,agent_name,total_talk_time,working_hour,leisure_hour,alarm,total_calls_landed,total_calls_taken,loginTime,logoutTime,officeTime,offlineTime
                    FROM (SELECT
                    		date(login_time) AS `login_date`,
                    		`company_agent_id` AS `agent_name`,
                    		Sec_to_Time(Sum(`talk_time`)) as `total_talk_time`,
                    		Sec_to_Time(SUM(`duration`)) as `working_hour`,
                    		Sec_to_Time(Sum(`duration` - `talk_time`)) as `leisure_hour`,
                    		SUM(`alarm`) as `alarm`,
                    		SUM(`calls_landed`) AS `total_calls_landed`,
                    		SUM(`calls_taken`) AS `total_calls_taken`,
                            MIN(`login_time`) AS `loginTime`,
                            MAX(`logout_time`) AS `logoutTime`,
                            TIMEDIFF(MAX(`logout_time`),MIN(`login_time`)) as officeTime,
                            TIMEDIFF(TIMEDIFF(MAX(`logout_time`),MIN(`login_time`)),Sec_to_Time(SUM(`duration`))) as offlineTime
                    	FROM `company_agent_log`
                    	WHERE
                    	    DATE(`login_time`) >='".$from_year."'
                            AND DATE(`logout_time`) <= '".$to_year."'".$where_query."
                    		AND `company_id` = ".$company_id."
                    	GROUP BY `company_agent_id`,date(login_time)
                    	ORDER BY `company_agent_id` ASC)agents ORDER BY login_date" ;

        $subData	=	$db->fetchAll( $query ) ;
        return $subData ;


    }

    function get_agent_detail(){
        $this->autoLayout = false;

        $date = $this->RequestHandler->params['form']['date'];
        $agent_id = $this->RequestHandler->params['form']['agent_id'];

        $db = ConnectionManager::getDataSource("vcc"); // name of your database connection
        $query = "  SELECT company_agent_id,login_time,logout_time,hms,calls_landed,calls_taken,talk_time,alarm
                    FROM company_agent_log
                    WHERE  company_agent_id = ".$agent_id." AND date(login_time)='".$date."'
                    ORDER BY login_time" ;
        $agent_logs	=	$db->fetchAll( $query ) ;
        $this->set(compact('agent_logs'));

    }
}