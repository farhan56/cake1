<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 27/08/2015
 * Time: 4:57 PM
 */


class LoginController extends AppController{

    var $name = 'Login';
    var $uses = array('User','Notice','Message');

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array('login', 'logout'));
    }

    function login($cli=null){
        $this->layout = 'login';
        if(!empty($this->data)){
            //echo "hello hi";
            if($this->Auth->login($this->data)){
                $this->User->recursive = 1;
                $user = $this->User->read(null, $this->Auth->user('id'));
                $notice = $this->Notice->find('count',array('conditions'=>array('to_user_id'=>$this->Auth->user('id'),'read'=>0)));
                $message=$this->Message->find('count',array('conditions'=>array('to_user_id'=>$this->Auth->user('id'),'read'=>0)));
                $this->Session->write('role',$user['UserRole']['name']);
                $this->Session->write('user_id',$user['User']['id']);
                $this->Session->write('login_id',$user['User']['login_id']);
                $this->Session->write('notice_count',$notice);
                $this->Session->write('message_count',$message);
                if($cli!=null)
                {
                    debug($this->params['url']['cli']);
                    if($user['UserRole']['name'] == 'Admin') {
                        //$this->redirect($this->Auth->loginRedirect);
                        //$this->redirect($this->referer());
                        $this->redirect(array('controller' => 'Home','action' => 'home'));

                    }
                    else if($user['UserRole']['name'] == 'Doctor'){
                        //$this->redirect(array('controller' => 'Home','action' => 'index'));
                        $this->redirect(array('controller' => 'Home','action' => 'home'));

                    }
                    else if($user['UserRole']['name'] == 'Agent'){
                        //$this->redirect(array('controller' => 'Home_Agent','action' => 'index'));
                        //$this->redirect($this->referer());
                        $this->redirect(array('controller' => 'HomeAgent','action' => 'homeagent'));

                    }
                    else {
                        $this->redirect(array('controller' => 'Services','action' => 'index'));
                    }
                }

//                if($user['UserRole']['name'] == 'Admin') {
//                    $this->redirect($this->Auth->loginRedirect);
//                } else if($user['UserRole']['name'] == 'Doctor'){
//                    $this->redirect(array('controller' => 'Home','action' => 'index'));
//                } else {
//                    $this->redirect(array('controller' => 'Services','action' => 'index'));
//                }
                $this->set('model', $this->Auth->userModel);
            }
        }

        if($this->Auth->user()) {

            if($this->Session->read('role') == 'Admin') {

                if (preg_match('/home/', $this->Session->read('lastUrl'))){
                    $this->redirect(array('controller' => 'Home','action' => 'home','?'=>array('req'=>end(explode('=',$this->Session->read('lastUrl'))))));
                }
                else
                    $this->redirect(array('controller' => 'Home','action' => 'home','?'=>array('req'=>"")));


            } else if($user['UserRole']['name'] == 'Supervisor'){
                if (preg_match('/home/', $this->Session->read('lastUrl'))) {
                    $this->redirect(array('controller' => 'Home', 'action' => 'home', '?' => array('req' => end(explode('=', $this->Session->read('lastUrl'))))));
                }

                else
                        $this->redirect(array('controller' => 'Home','action' => 'home','?'=>array('req'=>"")));

            } else if($this->Session->read('role') == 'Agent'){
                //$this->redirect(array('controller' => 'HomeAgent','action' => 'homeagent'));
                if (preg_match('/home/', $this->Session->read('lastUrl'))){
                    $this->redirect(array('controller' => 'HomeAgent','action' => 'homeagent','?'=>array('req'=>end(explode('=',$this->Session->read('lastUrl'))))));
                }
                else
                    $this->redirect(array('controller' => 'HomeAgent','action' => 'homeagent','?'=>array('req'=>"")));
            }
            else if($this->Session->read('role') == 'Doctor')
            {
                if (preg_match('/home/', $this->Session->read('lastUrl'))){
                    $this->redirect(array('controller' => 'Home','action' => 'home','?'=>array('req'=>end(explode('=',$this->Session->read('lastUrl'))))));
                }
                else
                    $this->redirect(array('controller' => 'Home','action' => 'home','?'=>array('req'=>"")));
            }
        }
    }

    function logout(){

        $this->Session->destroy();
        $this->Auth->logout();
        $this->redirect(array('action' => 'login'));
    }

}