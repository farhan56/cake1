<script type="text/javascript">

$(document).ready(function(){
        $('#UserRoleId').change(function(){
        console.log($(this).val());
         $("#doctor").css("display","none");
        if($(this).val()=='3')
        {

            $("#doctor").css("display","block");
        }
        else
        {
            $("#doctor").css("display","none");
        }

        });
        $('#UserImage').change(function(){
                console.log($(this).val());
            }
        )
});
</script>
<div class="services form">
<?php echo $this->Form->create('User', array('type' => 'file','url' => array('controller' => 'User', 'action' => 'add')));?>
	<fieldset>
		<legend><?php __('Add User'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('login_id',array('label'=>'Login','type'=>'text'));
		echo $this->Form->input('password',array('type'=>'password'));
		echo $this->Form->input('role_id',array('label'=>'Role','type'=>'select','options'=>$user_role,'default'=>'1'));
		echo $this->Form->input('created_on', array('style' => 'width:8%', 'default' => date('Y-m-d H:i:s')));
		echo $this->Form->input('update_date', array('style' => 'width:8%', 'default' => date('Y-m-d H:i:s')));
        echo $this->Form->input('is_active', array('type' => 'select', 'options' => array('Y' => 'Yes', 'N' => 'No')));

        echo $this->Form->input('address',array('label'=>'Address'));
        echo $this->Form->input('photo', array('label'=>'Upload Photo (Must be<2MB(.png,.jpg))','type' => 'file'));

        //echo $form->labelTag('File/image', 'Image');
        //echo $html->file('File/image');
        echo $this->Form->input('email_id',array('label'=>'Email','type'=>'text')); ?>

        <div id="doctor" style="display:none">

        <?php echo $this->Form->input('mbbs_year',array('label'=>'MBBS Year','default'=>''));
        echo $this->Form->input('college',array('label'=>'College','type'=>'text','default'=>'')); ?>
        </div>




	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>