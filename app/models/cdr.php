<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 30/08/2015
 * Time: 4:22 PM
 */

class Cdr extends AppModel{

    var $name = 'Cdr';
    public $validate = array(
        'msisdn' => array(
            'rule' =>  'Numeric',
            'required' => true,
            'message' => 'Numbers only'
        ),
        'age_year' => array(
            'rule' => array('minLength', 1),
            'required' => true,
            'message' => 'Enter age year',
            'allowEmpty' => false
        ),
        'age_month' => array(
            'rule' => array('minLength', 1),
            'required' => true,
            'message' => 'Enter age month',
            'allowEmpty' => false
        ),

        'call_region' => array(
            'rule' => array('minLength', 1),
            'required' => true,
            'message' => 'Enter call region',
            'allowEmpty' => false
        )
    );
    var $useTable = 'hlink_cdr_info';
    var $displayField = 'id';
    var $belongsTo =array(
        'User'=>array(
            'fields'=>array('name'),
            'className'=>'User',
            'foreignKey'=>'doctor_id'
        ),
        'Drug1'=>array(
            'fields'=>array('name'),
            'className'=>'Drug',
            'foreignKey'=>'drug1',
        ),
        'Drug2'=>array(
            'fields'=>array('name'),
            'className'=>'Drug',
            'foreignKey'=>'drug2'
        ),
        'Drug3'=>array(
            'fields'=>array('name'),
            'className'=>'Drug',
            'foreignKey'=>'drug3'
        ),

        'ProvisionalDiagnosis'=>array(
            'className'=>'ProvisionalDiagnosis',
            'foreignKey'=>'prv_diagnosis_id'
        )
    );



}