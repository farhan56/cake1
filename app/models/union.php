<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 30/08/2015
 * Time: 6:22 PM
 */

class Union extends AppModel{

    var $name = 'Union';
    var $useTable = 'hlink_union';
    var $displayField = 'name';
}