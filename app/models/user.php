<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 27/08/2015
 * Time: 5:08 PM
 */

class User extends AppModel{

    var $name = 'User';
    var $useTable ='hlink_user';
    public $validate = array(

        'email_id' => array(
            'rule' => array('minLength', 3),
            'required' => true,
            'message' => 'Enter email',
            'allowEmpty' => false
        )


    );
   var $belongsTo = array(
        'UserRole' => array(
            'className' => 'UserRole',
            'foreignKey' => 'role_id'
        ),
       'Cdr' => array(
           'className'=>'Cdr',
           'foreignKey'=>'id'

       )
    );

    function hashPasswords($data) {
        return $data;
    }

}