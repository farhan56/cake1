<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 30/08/2015
 * Time: 1:52 PM
 */

class ProvisionalDiagnosis extends AppModel{
    var $name = 'ProvisionalDiagnosis';
    var $useTable = 'hlink_prv_diagnosis';
    var $displayField  = 'disease_name';
    var $validate = array(
        'disease_name'=>array(
            'rule' => 'alphaNumeric',
            'required'=>true,
            'allowEmpty' => false,
            'message' => 'Disease Name is required'
        )
    );
//    var $hasMany = array(
//        'Drug' => array(
//            'className' => 'Drug',
//            'foreignKey' => 'provisional_diagnosis_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        )
//    );


}