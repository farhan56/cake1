<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 30/08/2015
 * Time: 2:06 PM
 */

class SpecialistController extends  AppController{

    var $name = 'Specialist';
    var $uses = array('Specialist','User');
    function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'setup';
    }
    function index(){
        $this->layout = 'setup';
        $this->set('specialists', $this->paginate());

    }

    function add(){

        if(!empty($this->data)){
            if ($this->Specialist->save($this->data)) {
                $this->Session->setFlash('New Specialization Type has been created.',true);
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__('The Provisional Diagnosis Name could not be saved. Please, try again.', true));

            }
        }
    }

    public function edit($id){

        $this->Specialist->id = $id;
        if(!empty($this->data)){
            if ($this->Specialist->save($this->data)) {
                $this->Session->setFlash('Specialization Type has been updated.');
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__('The Specialization Type Name could not be updated. Please, try again.', true));
            }

        }else{
            $this->Specialist->recursive = 1;
            $this->data = $this->Specialist->read();
        }
    }

    public function delete($id){
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Specialization Type', true));
            $this->redirect(array('action'=>'index'));
        }else if($this->Specialist->delete($id)){
            $this->Session->setFlash('The Specialization Type with id: ' . $id . ' has been deleted.');
            $this->redirect(array('action' => 'index'));
        }else{

            $this->Session->setFlash(__('The Specialization Type could not be Deleted. Please, try again.', true));
        }
    }




}