<?php

/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 01/09/2015
 * Time: 11:17 AM
 */
class NoticeController extends AppController
{
    var $name = 'Notice';
    var $uses = array('Notice', 'User');

    function beforeFilter()
    {
        parent::beforeFilter();
        $this->layout = 'default';
        $this->Auth->allow(array('view'));
    }

    function index()
    {
        $user_id = $this->Session->read('user_id');

        $role = $this->Session->read('role');

        if($role=='Admin')
        {
            $this->paginate = array('order'=>array('sent_on'=>'DESC'));

        }
        else
            $this->paginate = array('order'=>array('sent_on'=>'DESC'),'conditions'=>array('OR' =>array('to_user_id'=>$user_id, 'all'=>'1')));

        $this->set('notices', $this->paginate());
        $this->set('role', $role);

    }

    function view($id){
        $this->Notice->id = $id;
        if($this->Session->read('role')!='Admin')
        {
            $this->Notice->saveField('read', true);
        }

        $this->Session->write('notice_count',$this->Notice->find('count',array('conditions'=>array('to_user_id'=>$this->Auth->user('id'),'read'=>'0'))));
        $this->set('notice',$this->Notice->findById($id));

    }

    function add()
    {
        $user_id = $this->Session->read('user_id');
        $supervisors = $this->User->find('list',array('conditions'=>array('id !='=>$user_id)));

        $role = $this->Session->read('role');
        if (!empty($this->data)) {
            if ($this->Notice->save($this->data)) {
                $this->Session->setFlash('Notice Published.', true);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Notice Can\'t be published', true));

            }
        }
        $this->set('role',$role);
        $this->set('admins',$supervisors);
        $this->set('user_id',$user_id);
    }
}