<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 30/08/2015
 * Time: 3:02 PM
 */

class DrugController extends AppController{

    var $name ='Drug';
    var $uses =array('Drug','User');

    function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'setup';
    }
    function index(){
        $this->layout = 'setup';
        $this->Drug->recursive = 1;
        $this->set('drugs', $this->paginate());

    }


    public function add(){


        if(!empty($this->data)){
            if ($this->Drug->save($this->data)) {
                $this->Session->setFlash('New Drug has been created.',true);
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__('The Drug could not be saved. Please, try again.', true));

            }
        }


    }
    public function edit($id){

        $this->Drug->id = $id;
        if(!empty($this->data)){
            if ($this->Drug->save($this->data)) {
                $this->Session->setFlash('Drug name has been updated.');
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__('The Drug could not be updated. Please, try again.', true));
            }

        }else{
            $this->Drug->recursive = 1;
            $this->data = $this->Drug->read();
//            $provisional_diagnosis = $this->Drug->ProvisionalDiagnosis->find('list');
//            $this->set(compact('provisional_diagnosis'));
//
//            if($this->data['Drug']['provisional_diagnosis_id']){
//                $this->set('provisional_diagnosis_id',$this->data['Drug']['provisional_diagnosis_id']);
//
//            }else{
//                $this->set('provisional_diagnosis_id',null);
//            }

        }
    }

    public function delete($id){
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Drug', true));
            $this->redirect(array('action'=>'index'));
        }else if($this->Drug->delete($id)){
            $this->Session->setFlash('The Drug with id: ' . $id . ' has been deleted.');
            $this->redirect(array('action' => 'index'));
        }else{

            $this->Session->setFlash(__('The Provisional Diagnosis could not be Deleted. Please, try again.', true));
        }
    }

}