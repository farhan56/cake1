<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 27/08/2015
 * Time: 5:07 PM
 */


class UserController extends  AppController{
    var $name = 'User';
    var $uses = array('User','UserRole');
    function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'setup';
    }
    public function index(){
        $this->layout = 'setup';
        $this->User->recursive = 1;
        $this->set('users', $this->paginate());

    }

    public function add(){
        if(!empty($this->data)){
			if(!$this->uploadFile())
            {
                //$path = $this->User->find('list',array('fields'=>array('User.photo'),'conditions'=>array('id'=>$this->Session->read('user_id'))));
                $this->data['User']['photo']='';
				if ($this->User->save($this->data)) {
                $this->Session->setFlash('New User has been created.',true);
                $this->redirect(array('action' => 'index'));
				}else{

					$user_roles = $this->User->UserRole->find('list');
					$this->set('user_role',$user_roles);
					$this->Session->setFlash(__('The User could not be saved. Please, try again.', true));


				}

            }
			
            else if ($this->uploadFile() && $this->User->save($this->data)) {
                $this->Session->setFlash('New User has been created.',true);
                $this->redirect(array('action' => 'index'));
				}else{

					$user_roles = $this->User->UserRole->find('list');
					$this->set('user_role',$user_roles);
					$this->Session->setFlash(__('The User could not be saved. Please, try again.', true));


				}

        }else{
            $user_roles = $this->User->UserRole->find('list');
            $this->set('user_role',$user_roles);
        }


    }
    function uploadFile() {
        $file = $this->data['User'];

        debug($file['photo']['type']);
            if (($file['photo']['type']== 'image/jpeg' || 
			$file['photo']['type']== 'image/jpg' || 
			$file['photo']['type']== 'image/png') &&
                $file['photo']['size']< 3620888 && move_uploaded_file($this->data['User']['photo']['tmp_name'], APP.'webroot/img/'.$this->data['User']['photo']['name'])) {
					$this->data['User']['photo'] = $this->webroot."/app/webroot/img/".$this->data['User']['photo']['name'];

					return true;
            }

        return false;
    }
    public function edit($id){

        $this->User->id = $id;

        debug($id);

        if(!empty($this->data)){

            if (isset( $this->params['form']['cancel'])) {
                $this->Session->setFlash(__('Changes were not saved. Changes have been cancelled.', true));
                $this->redirect( array( 'controller' => 'Home', 'action' => 'home'));
            }

            if(!$this->uploadFile())
            {
                $path = $this->User->find('list',array('fields'=>array('User.photo'),'conditions'=>array('id'=>$this->Session->read('user_id'))));
                $this->data['User']['photo']=$path[1];

            }

            if ( $this->User->save($this->data)) {

                $this->Session->setFlash('User has been updated.');

                $this->redirect(array('action' => 'index'));
            }else{
                $this->User->recursive = 1;
                $this->data = $this->User->read();
                $user_roles=$this->User->UserRole->find('list');
                $role = $this->User->UserRole->find('list',array('conditions'=>array('id'=>$this->data['User']['role_id'])));
                $this->set('id',$id);
                $this->set('role',$role);
                $this->set('user_role',$user_roles);
                $this->set('role_id',$this->data['User']['role_id']);
                $this->set('photo',$this->data['User']['photo']);
                $this->Session->setFlash(__('The User could not be updated. Please, try again.', true));
            }

        }else{
            $this->User->recursive = 1;
            $this->data = $this->User->read();
            $user_roles=$this->User->UserRole->find('list');
            $role = $this->User->UserRole->find('list',array('conditions'=>array('id'=>$this->data['User']['role_id'])));
			debug($role);
            if($role[1]=='Admin')
            {
                $this->set('role',$role[1]);
            }
            else if($role[3]=='Doctor')
            {
                $this->set('role',$role[3]);
            }
            else if($role[4]=='Supervisor')
            {
                $this->set('role',$role[4]);
            }
            else if($role[5]=='Agent')
            {
                $this->set('role',$role[5]);
            }

            $this->set('id',$id);
            //$this->set('role',$role);
            $this->set('user_role',$user_roles);
            $this->set('role_id',$this->data['User']['role_id']);
            $this->set('photo',$this->data['User']['photo']);

        }
    }

    public function delete($id){
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for User', true));
            $this->redirect(array('action'=>'index'));
        }else if($this->User->delete($id)){
            $this->Session->setFlash('The User with id: ' . $id . ' has been deleted.');
            $this->redirect(array('action' => 'index'));
        }else{

            $this->Session->setFlash(__('The User could not be Deleted. Please, try again.', true));
        }
    }

    public function change(){
        $this->layout = 'default';
        $this->User->id = $this->Session->read('user_id');
        if(!empty($this->data)){
            if ($this->User->save($this->data)) {
                $this->Session->setFlash('Password has been updated.');
                $this->redirect(array('controller'=>'Home','action' => 'home'));
            }else{
                $this->Session->setFlash(__('Password could not be updated. Please, try again.', true));
            }

        }else{
            $this->User->recursive = 0;
            $this->data = $this->User->read();
        }

    }
    public function profile(){
        $this->layout = 'default';
        //$this->User->id = $this->Session->read('user_id');

//        $this->User->recursive =-1;
        $user = $this->User->find('all',array('recursive'=>-1,'conditions'=>array('id'=>$this->Session->read('user_id'))));
        //debug($user);
        $this->set('users',$user);
    }


}