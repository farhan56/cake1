<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 01/09/2015
 * Time: 12:58 PM
 */

class MessageController extends AppController{

    var $name = 'Message';
    var $uses = array('Message','User');
    function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(array('view_inbox'));
        $this->layout = 'default';
    }

    function index()
    {
        $user_id = $this->Session->read('user_id');
        $this->paginate = array('order'=>array('category'=>'DESC','sent_on'=>'DESC'),'conditions'=>array('from_user_id'=>$user_id));
        $this->set('sent_messages', $this->paginate());

        //$this->set('messages', $this->paginate());
        $this->paginate = array('order'=>array('category'=>'DESC','sent_on'=>'DESC'),'conditions'=>array('OR'=>(array('to_user_id'=>$user_id,'all'=>'1'))));
        $this->set('inbox', $this->paginate());


    }

    function add()
    {
        $user_id = $this->Session->read('user_id');
        $supervisors = $this->User->find('list',array('conditions'=>array('id !='=>$user_id)));

        $role = $this->Session->read('role');
        if (!empty($this->data)) {
            if ($this->Message->save($this->data)) {
                $this->Session->setFlash('Message has been sent.', true);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('User Role could not be saved. Please, try again.', true));

            }
        }
        $this->set('role',$role);
        $this->set('admins',$supervisors);
        $this->set('user_id',$user_id);
    }
    function reply($id)
    {
        $user_id = $this->Session->read('user_id');

        $supervisors = $this->User->find('list',array('conditions'=>array('id'=>$id)));

        $role = $this->Session->read('role');
        if (!empty($this->data)) {
            if ($this->Message->save($this->data)) {
                $this->Session->setFlash('Message has been sent.', true);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Message could not be sent. Please, try again.', true));

            }
        }
        $this->set('role',$role);
        $this->set('admins',$supervisors);
        $this->set('user_id',$user_id);
        $this->render('add');
    }
    function view($id){

        $this->set('message',$this->Message->findById($id));


    }
    function view_inbox($id){
        $this->Message->id = $id;
        $this->Message->saveField('read', true);
        $this->Session->write('message_count',$this->Message->find('count',array('conditions'=>array('OR' =>array('to_user_id'=>$this->Auth->user('id'),'all'=>'1'),'read'=>'0'))));
        $this->set('message',$this->Message->findById($id));
    }
}