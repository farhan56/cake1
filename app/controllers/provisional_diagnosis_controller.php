<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 30/08/2015
 * Time: 1:54 PM
 */

class ProvisionalDiagnosisController extends AppController{

    var $name = 'ProvisionalDiagnosis';
    var $uses = array('ProvisionalDiagnosis','User');

    function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'setup';
    }
    public function index(){

        $this->set('diagnosises', $this->paginate());
    }

    public function add(){

        if(!empty($this->data)){
            if ($this->ProvisionalDiagnosis->save($this->data)) {
                $this->Session->setFlash('New Provisional Diagnosis Name has been created.',true);
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__('The Provisional Diagnosis Name could not be saved. Please, try again.', true));

            }
        }



    }
    public function edit($id){

        $this->ProvisionalDiagnosis->id = $id;
        if(!empty($this->data)){
            if ($this->ProvisionalDiagnosis->save($this->data)) {
                $this->Session->setFlash('Provisional Diagnosis Name has been updated.');
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__('The Provisional Diagnosis Name could not be updated. Please, try again.', true));
            }

        }else{
            $this->ProvisionalDiagnosis->recursive = 1;
            $this->data = $this->ProvisionalDiagnosis->read();
        }
    }

    public function delete($id){
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Provisional Diagnosis', true));
            $this->redirect(array('action'=>'index'));
        }else if($this->ProvisionalDiagnosis->delete($id)){
            $this->Session->setFlash('The Provisional Diagnosis with id: ' . $id . ' has been deleted.');
            $this->redirect(array('action' => 'index'));
        }else{

            $this->Session->setFlash(__('The Provisional Diagnosis could not be Deleted. Please, try again.', true));
        }
    }


}