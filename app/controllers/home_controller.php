<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 27/08/2015
 * Time: 6:57 PM
 */

class HomeController extends AppController{

    var $name = 'Home';
    var $uses = array('Cdr','User','ProvisionalDiagnosis','Specialist','Drug','District','Thana','Union');

    function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'default';
    }
    function home(){

        $cli = $this->params['url']['req'];
        $user_id = $this->Session->read('user_id');
        $this->set(compact('cli'));
        $this->set(compact('user_id'));
        $this->set('districts',$this->District->find('list',array('order'=>array('name'))));
        //$this->set('thanas', $this->Thana->find('list'));
        //echo $this->Thana->find('list');
        //$this->set('thanas',$this->Thana->find('list'));
        //$this->set('unions',$this->Union->find('list'));
        $this->set('diagnosises',$this->ProvisionalDiagnosis->find('list',array('order'=>array('disease_name'))));
        $this->set('specialists',$this->Specialist->find('list'));
        $this->set('drugs',$this->Drug->find('list'));
        $this->Cdr->recursive = 1;
        $this->set('caller_history',$this->paginate('Cdr',array('msisdn'=>$cli)));


    }

    function add(){
//        debug($this->RequestHandler->params['form']);

        if(!empty($this->RequestHandler->params['form'])){

            if ($this->Cdr->save($this->RequestHandler->params['form'])) {
                $this->Session->setFlash('New Call Detail Record has been created.',true);
                $this->redirect(array('action' => 'home','?'=>array('req'=>$this->RequestHandler->params['form']['msisdn'])));
            }else{

                $this->Session->setFlash(__('The Call Detail Record could not be saved. Please fill the mandatory fields(*), try again.', true));
                $this->redirect(array('action' => 'home','?'=>array('req'=>$this->RequestHandler->params['form']['msisdn'])));
            }
        }

    }

    public function getByCategory() {
        /*$district_id = $this->request->data['Get']['district_id'];
        //echo $district_id;
        $thanas = $this->Thana->find('list', array(
            'conditions' => array('Thana.district_id' => $district_id),
            'recursive' => -1));
        */
        $this->layout = false;
        $this->autoRender = false;
        $district_id = $_GET['dist_id'];
        $thanas = $this->Thana->find('list', array(
            'conditions' => array('Thana.district_id' => $district_id),
            'recursive' => -1));

        //foreach($thanas as $key => $value):
          //          <option value="<?php echo $key;
        //         endforeach;
        echo $thanas;
        $this->set('thanas', $this->Thana->find('list', array(
            'conditions' => array('Thana.district_id' => $district_id),
            'recursive' => -1)));


    }
    function getLocation(){
        $this->layout = 'ajax';
        if(($this->RequestHandler->params['form']['type']=='thana') &&($this->RequestHandler->params['form']['district_id']>0)){
            $this->set('thanas',$this->Thana->find('list',array('conditions'=>array('district_id'=>$this->RequestHandler->params['form']['district_id']),'order '=>array('name'))));
        }else if(($this->RequestHandler->params['form']['type']=='union') &&($this->RequestHandler->params['form']['thana_id']>0)){
            $this->set('unions',$this->Union->find('list',array('conditions'=>array('thana_id'=>$this->RequestHandler->params['form']['thana_id']),'order'=>array('name'))));
        }


    }
    function doctor(){


    }

}