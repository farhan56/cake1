<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 27/08/2015
 * Time: 6:57 PM
 */

class HomeAgentController extends AppController{


    var $name = 'HomeAgent';
    var $uses = array('Cdragent','User','District','Thana','Union');

    function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'default';
    }
    function homeagent(){

        $cli = $this->params['url']['req'];
        $user_id = $this->Session->read('user_id');
        $this->set(compact('cli'));
        $this->set(compact('user_id'));
        $this->set('districts',$this->District->find('list'));
        //$this->set('diagnosises',$this->ProvisionalDiagnosis->find('list'));
        //$this->set('specialists',$this->Specialist->find('list'));
        //$this->set('drugs',$this->Drug->find('list'));

        $this->Cdragent->recursive = 1;
        $this->set('caller_history',$this->paginate($this->Cdragent->find('list',array('conditions'=>array('msisdn'=>$cli)))));


    }

    function add(){
         debug($this->RequestHandler->params['form']);
        if(!empty($this->RequestHandler->params['form'])){

            if ($this->Cdragent->save($this->RequestHandler->params['form'])) {
                $this->Session->setFlash('New Call Detail Record has been created.',true);
                $this->redirect(array('action' => 'homeagent','?'=>array('req'=>$this->RequestHandler->params['form']['msisdn'])));
            }else{


                //echo "hi";
                //$errors = $this->cdragent->validationErrors;

                $this->Session->setFlash(__('The Call Detail Record could not be saved. Please, try again and fillup the mandatory(*) fields.', true));
                $this->redirect(array('action' => 'homeagent','?'=>array('req'=>$this->RequestHandler->params['form']['msisdn'])));

            }
        }

    }
    function getLocation(){
        $this->layout = 'ajax';
        if(($this->RequestHandler->params['form']['type']=='thana') &&($this->RequestHandler->params['form']['district_id']>0)){
            $this->set('thanas',$this->Thana->find('list',array('conditions'=>array('district_id'=>$this->RequestHandler->params['form']['district_id']))));
        }else{
            $this->set('unions',$this->Union->find('list',array('conditions'=>array('thana_id'=>$this->RequestHandler->params['form']['thana_id']))));
        }


    }

}